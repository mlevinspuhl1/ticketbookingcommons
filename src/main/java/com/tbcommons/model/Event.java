/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tbcommons.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author
 */
@Entity
@Table(name = "event")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Event.findAll", query = "SELECT e FROM Event e")
    , @NamedQuery(name = "Event.findById", query = "SELECT e FROM Event e WHERE e.id = :id")
    , @NamedQuery(name = "Event.findByType", query = "SELECT e FROM Event e WHERE e.type = :type")
    , @NamedQuery(name = "Event.findByDate", query = "SELECT e FROM Event e WHERE e.date = :date")
    , @NamedQuery(name = "Event.findByAdultTicketPrice", query = "SELECT e FROM Event e WHERE e.adultTicketPrice = :adultTicketPrice")
    , @NamedQuery(name = "Event.findByKIDSTicketPrice", query = "SELECT e FROM Event e WHERE e.kIDSTicketPrice = :kIDSTicketPrice")
    , @NamedQuery(name = "Event.findByTeenagerTicketPrice", query = "SELECT e FROM Event e WHERE e.teenagerTicketPrice = :teenagerTicketPrice")
    , @NamedQuery(name = "Event.findByStudentTicketPrice", query = "SELECT e FROM Event e WHERE e.studentTicketPrice = :studentTicketPrice")
    , @NamedQuery(name = "Event.findByElderlyTicketPrice", query = "SELECT e FROM Event e WHERE e.elderlyTicketPrice = :elderlyTicketPrice")
    , @NamedQuery(name = "Event.findByCategory", query = "SELECT e FROM Event e WHERE e.category = :category")})
public class Event implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "adultTicketPrice")
    private BigDecimal adultTicketPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "KIDSTicketPrice")
    private BigDecimal kIDSTicketPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "teenagerTicketPrice")
    private BigDecimal teenagerTicketPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "studentTicketPrice")
    private BigDecimal studentTicketPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "elderlyTicketPrice")
    private BigDecimal elderlyTicketPrice;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "category")
    private String category;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eventId")
    private List<Ticket> ticketList;
    @JoinColumn(name = "artist_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Artist artistId;
    @JoinColumn(name = "venue_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Venue venueId;

    public Event() {
    }

    public Event(Long id) {
        this.id = id;
    }

    public Event(Long id, String type, Date date, BigDecimal adultTicketPrice, BigDecimal kIDSTicketPrice, BigDecimal teenagerTicketPrice, BigDecimal studentTicketPrice, BigDecimal elderlyTicketPrice, String category) {
        this.id = id;
        this.type = type;
        this.date = date;
        this.adultTicketPrice = adultTicketPrice;
        this.kIDSTicketPrice = kIDSTicketPrice;
        this.teenagerTicketPrice = teenagerTicketPrice;
        this.studentTicketPrice = studentTicketPrice;
        this.elderlyTicketPrice = elderlyTicketPrice;
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAdultTicketPrice() {
        return adultTicketPrice;
    }

    public void setAdultTicketPrice(BigDecimal adultTicketPrice) {
        this.adultTicketPrice = adultTicketPrice;
    }

    public BigDecimal getKIDSTicketPrice() {
        return kIDSTicketPrice;
    }

    public void setKIDSTicketPrice(BigDecimal kIDSTicketPrice) {
        this.kIDSTicketPrice = kIDSTicketPrice;
    }

    public BigDecimal getTeenagerTicketPrice() {
        return teenagerTicketPrice;
    }

    public void setTeenagerTicketPrice(BigDecimal teenagerTicketPrice) {
        this.teenagerTicketPrice = teenagerTicketPrice;
    }

    public BigDecimal getStudentTicketPrice() {
        return studentTicketPrice;
    }

    public void setStudentTicketPrice(BigDecimal studentTicketPrice) {
        this.studentTicketPrice = studentTicketPrice;
    }

    public BigDecimal getElderlyTicketPrice() {
        return elderlyTicketPrice;
    }

    public void setElderlyTicketPrice(BigDecimal elderlyTicketPrice) {
        this.elderlyTicketPrice = elderlyTicketPrice;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @XmlTransient
    @JsonIgnore
    public List<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }

    public Artist getArtistId() {
        return artistId;
    }

    public void setArtistId(Artist artistId) {
        this.artistId = artistId;
    }

    public Venue getVenueId() {
        return venueId;
    }

    public void setVenueId(Venue venueId) {
        this.venueId = venueId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Event)) {
            return false;
        }
        Event other = (Event) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tbcommons.model.Event[ id=" + id + " ]";
    }
    
}
