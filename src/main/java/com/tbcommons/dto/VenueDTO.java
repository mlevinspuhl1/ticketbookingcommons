/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tbcommons.dto;

import com.tbcommons.enums.Enums.ResponseEnum;
import com.tbcommons.model.Venue;

public class VenueDTO extends ApiDTO {

    private Venue venue;

    public VenueDTO() {
    }

    public VenueDTO(ResponseEnum response, String message) {
        super(response, message);
    }

    public VenueDTO(ResponseEnum response, String message, Venue venue) {
        super(response, message);
        this.venue = venue;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }
    
}
