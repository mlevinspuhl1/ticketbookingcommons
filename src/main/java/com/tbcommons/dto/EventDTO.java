/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tbcommons.dto;

import com.tbcommons.enums.Enums;
import com.tbcommons.model.Event;

/**
 *
 * @author
 */
public class EventDTO extends ApiDTO {

    private Event event;

    public EventDTO() {
    }

    public EventDTO(Enums.ResponseEnum response, String message) {
        super(response, message);
    }

    public EventDTO(Enums.ResponseEnum response, String message, Event event) {
        super(response, message);
        this.event = event;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

}
