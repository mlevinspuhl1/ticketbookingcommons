/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tbcommons.dto;

import com.tbcommons.enums.Enums.ResponseEnum;
import com.tbcommons.model.Ticket;
import java.util.List;

/**
 *
 * @author
 */
public class TicketDTO extends ApiDTO {

    private Ticket ticket;
    private List<Ticket> ticketList;

    public TicketDTO() {
    }

    public TicketDTO(ResponseEnum response, String message) {
        super(response, message);
    }

    public TicketDTO(ResponseEnum response, String message, Ticket ticket) {
        super(response, message);
        this.ticket = ticket;
    }

    public TicketDTO(ResponseEnum response, String message, List<Ticket> ticketList) {
        super(response, message);
        this.ticketList = ticketList;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public List<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }

}
