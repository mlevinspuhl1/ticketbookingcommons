/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tbcommons.dto;

import com.tbcommons.enums.Enums.ResponseEnum;
import com.tbcommons.model.Customer;
import com.tbcommons.model.Ticket;
import java.util.List;

/**
 *
 * @author
 */
public class CustomerDTO extends ApiDTO {

    private Customer customer;
    private List<Ticket> ticketList;

    public CustomerDTO() {
    }

    public CustomerDTO(ResponseEnum response, String message) {
        super(response, message);
    }

    public CustomerDTO(ResponseEnum response, String message, Customer customer) {
        super(response, message);
        this.customer = customer;
    }

    public CustomerDTO(ResponseEnum response, String message,
            Customer customer, List<Ticket> ticketList) {
        super(response, message);
        this.customer = customer;
        this.ticketList=ticketList;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }
    

}
