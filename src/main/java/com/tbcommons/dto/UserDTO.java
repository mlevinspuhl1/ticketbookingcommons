/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tbcommons.dto;

import com.tbcommons.enums.Enums.ResponseEnum;
import com.tbcommons.model.Artist;
import com.tbcommons.model.Customer;
import com.tbcommons.model.Employee;
import com.tbcommons.model.User;

public class UserDTO extends ApiDTO {

    private User user;
    private Artist artist;
    private Employee employee;
    private Customer customer;

    public UserDTO() {
    }

    public UserDTO(ResponseEnum response, String message) {
        super(response, message);
    }

    public UserDTO(ResponseEnum response, String message, User user) {
        super(response, message);
        this.user = user;
    }

    public UserDTO(ResponseEnum responseEnum, String label, User user,
            Artist artist, Customer customer, Employee employee) {
        this.user = user;
        this.artist = artist;
        this.customer = customer;
        this.employee = employee;

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
}
