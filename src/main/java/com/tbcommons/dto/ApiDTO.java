/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tbcommons.dto;

import com.tbcommons.enums.Enums.ResponseEnum;

/**
 *
 *
 */
public class ApiDTO {

    private ResponseEnum response;
    private String message;

    public ApiDTO() {
    }

    public ApiDTO(ResponseEnum response, String message) {
        this.response = response;
        this.message = message;
    }

    public ResponseEnum getResponse() {
        return response;
    }

    public void setResponse(ResponseEnum response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
