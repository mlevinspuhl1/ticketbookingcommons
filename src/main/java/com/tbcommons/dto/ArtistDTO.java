/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tbcommons.dto;

import com.tbcommons.enums.Enums;
import com.tbcommons.model.Artist;

/**
 *
 * @author genef
 */
public class ArtistDTO extends ApiDTO{
    
    private Artist artist;
    
    public ArtistDTO(){        
    }
    
    public ArtistDTO(Enums.ResponseEnum response, String message){
        super (response, message);
    }
    
    public ArtistDTO(Enums.ResponseEnum response, String message, Artist artist){
        super (response, message);
        this.artist = artist;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }   
    
}
