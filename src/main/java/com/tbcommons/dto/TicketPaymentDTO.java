/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tbcommons.dto;

import com.tbcommons.model.Payment;
import com.tbcommons.model.Ticket;
import java.util.List;

/**
 *
 * @author
 */
public class TicketPaymentDTO {
    private List<Ticket> ticketList;
    private Payment payment;

    public TicketPaymentDTO() {
    }

    public TicketPaymentDTO(List<Ticket> ticketList, Payment payment) {
        this.ticketList = ticketList;
        this.payment = payment;
    }

    public List<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
    
}
