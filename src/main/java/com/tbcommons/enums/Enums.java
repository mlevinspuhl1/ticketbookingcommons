/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tbcommons.enums;

/**
 *
 */
public class Enums {

    public enum ResponseEnum {
        SUCCESS("success"),
        FAIL("fail");

        public final String label;

        private ResponseEnum(String label) {
            this.label = label;
        }
    }

    public enum UserStatusEnum {
        ACTIVE("Active"),
        INACTIVE("Inactive"),
        BANNED("Banned");
        public final String label;

        private UserStatusEnum(String label) {
            this.label = label;
        }
    }
}
