/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tbcommons.constant;

/**
 *
 */
public class Constants {

    public static final String INVALID_EMAIL = "Invalid email";
    public static final String INVALID_LOGIN = "Invalid login";
    public static final String INVALID_EMAIL_PASSWORD_LOGIN = "Email or password is not valid";
    public static final String EMAIL_SENT_TO_CONFIRM = "An email was sent for confirmation";
    public static final String PLEASE_SELECT_ONE_ROW = "Please select one row of the table";
    public static final String PAYMENT_TYPE_REQUIRED = "Payment type is required";
    public static final String TICKET_DELETED = "The ticket was deleted";
    public static final String EMAIL_NOT_SENT = "The email was not sent, please contact the admin";
    public static final String LOGIN_INACTIVE = "User is inactive";
    public static final String LOGIN_TRY_AGAIN = "Login error, please try again later";
    public static final String PERSISTENCE_UNIT_NAME = "TicketBookingWebPU";
    public static final String TRY_AGAIN = "Please try again later";
    public static final String CONTACT_ADMIN = "Please contact the administrator";
    public static final String ALREADY_EXISTS_EMAIL = "Email already exists";
    public static final String URL_API = "http://localhost:8080/TicketBookingWeb/api";
    public static final String USER_EMAIL_PATH = "user/email";
    public static final String ACTIVE_USER_EMAIL_PATH = "user/email/active";
    public static final String USER_API_PATH = "user";
    public static final String VENUE_API_PATH = "venue";
    public static final String EVENT_API_PATH = "event";
    public static final String EVENT_ID_ARTIST_API_PATH = "event/artist";
    public static final String ARTIST_API_PATH = "artist";
    public static final String TICKET_API_PATH = "ticket";
    public static final String CUSTOMER_API_PATH = "customer";
    public static final String VENUE_REMOVE_API_PATH = "venue/remove";
    public static final String EVENT_REMOVE_API_PATH = "event/remove";
    public static final String TICKET_REMOVE_API_PATH = "ticket/remove";
    public static final String CUSTOMER_REMOVE_API_PATH = "customer/remove";
    public static final String ARTIST_REMOVE_API_PATH = "artist/remove";
    public static final String USER_LOGIN_PATH = "user/login";
    public static final String USER_SAVE_PATH = "user/save";
    public static final String VENUE_SAVE_PATH = "venue/save";
    public static final String EVENT_SAVE_PATH = "event/save";
    public static final String ARTIST_SAVE_PATH = "artist/save";
    public static final String TICKET_SAVE_PATH = "ticket/save";
    public static final String TICKET_UPDATE_PRINTED_PATH = "ticket/update/printed";
    public static final String TICKET_SAVE_ALL_PATH = "ticket/saveAll";
    public static final String CUSTOMER_SAVE_PATH = "artist/save";
    public static final String USER_UPDATE_PATH = "user/update";
    public static final String VENUE_UPDATE_PATH = "venue/update";
    public static final String EVENT_UPDATE_PATH = "event/update";
    public static final String ARTIST_UPDATE_PATH = "artist/update";
    public static final String USER_ARTIST_UPDATE_PATH = "user/artist/update";
    public static final String TICKET_UPDATE_PATH = "ticket/update";
    public static final String CUSTOMER_UPDATE_PATH = "customer/update";
    public static final String EMPLOYEE_UPDATE_PATH = "user/employee/update";
    public static final String USER_CUSTOMER_UPDATE_PATH = "user/customer/update";
    public static final String USER_UPDATED_SUCCESS = "User updated";
    public static final String VENUE_UPDATED_SUCCESS = "Venue updated";
    public static final String EVENT_UPDATED_SUCCESS = "Event updated";
    public static final String TICKET_ADDED_SUCCESS = "Ticket added";
    public static final String THANKS_PAYMENT = "Thanks for the payment";
    public static final String EMPLOYEE_UPDATED_SUCCESS = "Employee updated";
    public static final String CONFIRM_EMAIL_PASSWORD = "Are email and password correct?";
    public static final String CONFIRM_NO_VENUE = "No venue... Is it correct??";
    public static final String CONFIRM_DELETE_VENUE = "Do you want to delete venue?";
    public static final String CONFIRM_DELETE_EVENT = "Do you want to delete event?";
    public static final String CONFIRM_DELETE_TICKET = "Do you want to delete ticket?";
    public static final String SELECT_AN_OPTION = "Select an Option...";
    public static final String SELECT_SELECT = "Select...";
    public static final String ADMIN = "Admin";
    public static final String SALE_ASSISTANT = "SaleAssistant";
    public static final String EMPLOYEE = "Employee";
    public static final String CUSTOMER = "Customer";
    public static final String ARTIST = "Artist";
    public static final String USER = "User";
    public static final String REGISTER = "UserRegister";
    public static final String VENUE = "Venue";
    public static final String EVENT = "Event";
    public static final String TICKET = "Ticket";
    public static final String PAYMENT = "Payment";
    public static final String PREFERENCE_EMAIL = "PreferenceEmail";
    public static final String FEMALE = "Female";
    public static final String MALE = "Male";
    public static final String OTHER = "Other";
    public static final String ADULT = "Adult";
    public static final String STUDENT = "Student";
    public static final String KIDS = "Kids";
    public static final String TEENAGER = "Teenager";
    public static final String ELDERLY = "Elderly";
}
